package mx.itesm.gerihealth;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

public class KegelExerciseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kegel_exercise);

        final MediaPlayer countdown1 = MediaPlayer.create(getApplicationContext(), R.raw.beep_countdown);
        final MediaPlayer countdown2 = MediaPlayer.create(getApplicationContext(), R.raw.beep_countdown);
        final MediaPlayer end = MediaPlayer.create(getApplicationContext(), R.raw.beep_end);

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            int count;

            @Override
            public void run() {
                if (count < 5)
                {
                    count++;

                    Timer beepTimer = new Timer();
                    beepTimer.scheduleAtFixedRate(new TimerTask() {
                        int beepCount;

                        @Override
                        public void run() {
                            if (beepCount < 9)
                            {
                                if (count % 2 != 0)
                                {
                                    if (countdown1.isPlaying()) countdown2.start();
                                    else countdown1.start();
                                }
                            }
                            else
                            {
                                end.start();
                                this.cancel();
                            }
                            beepCount++;
                        }
                    }, 0, 1000);


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (count % 2 != 0)
                            {
                                View relaje = findViewById(R.id.kegel_relaje);
                                relaje.setVisibility(View.GONE);
                                View contraiga = findViewById(R.id.kegel_contraiga);
                                contraiga.setVisibility(View.VISIBLE);
                            }
                            else
                            {
                                View relaje = findViewById(R.id.kegel_relaje);
                                relaje.setVisibility(View.VISIBLE);
                                View contraiga = findViewById(R.id.kegel_contraiga);
                                contraiga.setVisibility(View.GONE);
                            }
                        }
                    });
                }
                else
                {
                    this.cancel();
                    Globals.wakeLock.release();

                    Intent intent = new Intent(KegelExerciseActivity.this, KegelEndActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
            }
        }, 0, 11000);
    }
}
