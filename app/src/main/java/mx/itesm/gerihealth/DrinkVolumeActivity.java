package mx.itesm.gerihealth;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class DrinkVolumeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_volume);

        Button DrinkFinish = (Button) findViewById(R.id.Drink_Finish);
        DrinkFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (DrinkVolumeActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
