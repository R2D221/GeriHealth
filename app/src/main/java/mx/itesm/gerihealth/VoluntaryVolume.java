package mx.itesm.gerihealth;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class VoluntaryVolume extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voluntary_volume);

        Button VolumeNext = (Button) findViewById(R.id.Volume_Next);
        VolumeNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VoluntaryVolume.this, UrgencyActivity.class);
                startActivity(intent);
            }
        });
    }
}
