package mx.itesm.gerihealth;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;

public class KegelFirstAlarmActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kegel_first_alarm);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button next = (Button)findViewById(R.id.kegel_first_next);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TimePicker alarm = (TimePicker)findViewById(R.id.kegel_first_alarm);
                int hour;
                int minute;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    hour = alarm.getHour();
                    minute = alarm.getMinute();
                }
                else {
                    hour = alarm.getCurrentHour();
                    minute = alarm.getCurrentMinute();
                }

                SharedPreferences prefs = getSharedPreferences(getString(R.string.prefs), 0);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt(getString(R.string.prefs_first_alarm_hour), hour);
                editor.putInt(getString(R.string.prefs_first_alarm_minute), minute);
                editor.apply();

                Intent intent = new Intent(KegelFirstAlarmActivity.this, KegelSecondAlarmActivity.class);
                startActivity(intent);

            }
        });
    }

}
