package mx.itesm.gerihealth;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

public class DrinkSetActivity extends AppCompatActivity
    implements AdapterView.OnItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_set);

        Spinner Others = (Spinner) findViewById(R.id.SpinnerDrinkList);
        Others.setOnItemSelectedListener(this);

        Button DrinkNext = (Button) findViewById(R.id.Drink_Next);
        DrinkNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                Intent intent = new Intent(DrinkSetActivity.this, DrinkVolumeActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position == 4) {
            findViewById(R.id.OthersTextBox).setVisibility(View.VISIBLE);
            findViewById(R.id.OthersQ).setVisibility(View.VISIBLE);
        }
        else {
            findViewById(R.id.OthersTextBox).setVisibility(View.GONE);
            findViewById(R.id.OthersQ).setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
