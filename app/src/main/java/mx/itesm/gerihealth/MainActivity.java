package mx.itesm.gerihealth;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_LAYOUT_ID = "layout_id";

        private static final List<Integer> FRAGMENT_IDS = new ArrayList<Integer>()
            {{
                add(R.layout.content_urine);
                add(R.layout.content_diabetes);
            }};

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_LAYOUT_ID, FRAGMENT_IDS.get(sectionNumber));
            fragment.setArguments(args);
            return fragment;
        }

        private View onCreateUrineView(final View rootView)
        {
            SharedPreferences prefs = rootView.getContext().getSharedPreferences(getString(R.string.prefs), 0);
            TextView welcome = (TextView)rootView.findViewById(R.id.urine_welcome);

            welcome.setText(getString(R.string.urine_welcome, prefs.getString(getString(R.string.prefs_name), null)));

            if (!prefs.getBoolean(getString(R.string.prefs_kegel_active), false)) {
                rootView.findViewById(R.id.urine_kegel).setVisibility(View.GONE);
                rootView.findViewById(R.id.urine_activate_kegel).setVisibility(View.VISIBLE);
            }

            Button kegel = (Button)rootView.findViewById(R.id.urine_activate_kegel);
            kegel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(rootView.getContext(), KegelFirstAlarmActivity.class);
                    startActivity(intent);
                }
            });
            Button kegelNow = (Button)rootView.findViewById(R.id.urine_start_kegel);
            kegelNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(rootView.getContext(), KegelStartExerciseActivity.class);
                    startActivity(intent);
                }
            });
            Button Register = (Button)rootView.findViewById(R.id.RegisterEmission);
            Register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(rootView.getContext(), RegisterEmissionActivity.class);
                    startActivity(intent);
                }
            });
            Button Register2 = (Button)rootView.findViewById(R.id.TimeRegister);
            Register2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(rootView.getContext(), TimeRegisterActivity.class);
                    startActivity(intent);
                }
            });

            TextView nextAlarm = (TextView)rootView.findViewById(R.id.urine_latest_info);

            int firstHour = prefs.getInt(getString(R.string.prefs_first_alarm_hour), 0);
            int firstMinute = prefs.getInt(getString(R.string.prefs_first_alarm_minute), 0);
            int secondHour = prefs.getInt(getString(R.string.prefs_second_alarm_hour), 0);
            int secondMinute = prefs.getInt(getString(R.string.prefs_second_alarm_minute), 0);
            int thirdHour = prefs.getInt(getString(R.string.prefs_third_alarm_hour), 0);
            int thirdMinute = prefs.getInt(getString(R.string.prefs_third_alarm_minute), 0);

            Calendar now = Calendar.getInstance();

            if (now.get(Calendar.HOUR_OF_DAY) < firstHour)
            {
                nextAlarm.setText(String.format("%d:%02d", firstHour, firstMinute));
            }
            else
            if (now.get(Calendar.HOUR_OF_DAY) < secondHour)
            {
                nextAlarm.setText(String.format("%d:%02d", secondHour, secondMinute));
            }
            else
            if (now.get(Calendar.HOUR_OF_DAY) < thirdHour)
            {
                nextAlarm.setText(String.format("%d:%02d", thirdHour, thirdMinute));
            }
            else
            {
                nextAlarm.setText(String.format("%d:%02d", firstHour, firstMinute));
            }

            return rootView;
        }

        private View onCreateDiabetesView(final View rootView)
        {
            Button register = (Button)rootView.findViewById(R.id.diabetes_register);
            register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(rootView.getContext(), DiabetesRegisterActivity.class);
                    startActivity(intent);
                }
            });

            return rootView;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(getArguments().getInt(ARG_LAYOUT_ID), container, false);

            switch (getArguments().getInt(ARG_LAYOUT_ID))
            {
                case R.layout.content_urine:
                    return onCreateUrineView(rootView);
                case R.layout.content_diabetes:
                    return onCreateDiabetesView(rootView);
            }

            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Incontinencia";
                case 1:
                    return "Diabetes";
            }
            return null;
        }
    }
}
