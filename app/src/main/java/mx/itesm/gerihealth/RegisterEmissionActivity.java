package mx.itesm.gerihealth;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class RegisterEmissionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_emission);

        Button EmissionButton = (Button) findViewById(R.id.Emission);
        EmissionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterEmissionActivity.this, EmissionSetActivity.class);
                startActivity(intent);
            }
        });

        Button DrinkButton = (Button) findViewById(R.id.Drink);
        DrinkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterEmissionActivity.this, DrinkSetActivity.class);
                startActivity(intent);
            }
        });
    }
}

