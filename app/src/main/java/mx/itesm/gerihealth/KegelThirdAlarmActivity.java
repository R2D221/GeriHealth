package mx.itesm.gerihealth;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class KegelThirdAlarmActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kegel_third_alarm);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button next = (Button)findViewById(R.id.kegel_third_next);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TimePicker alarm = (TimePicker) findViewById(R.id.kegel_third_alarm);
                int hour;
                int minute;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    hour = alarm.getHour();
                    minute = alarm.getMinute();
                } else {
                    hour = alarm.getCurrentHour();
                    minute = alarm.getCurrentMinute();
                }

                SharedPreferences prefs = getSharedPreferences(getString(R.string.prefs), 0);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt(getString(R.string.prefs_third_alarm_hour), hour);
                editor.putInt(getString(R.string.prefs_third_alarm_minute), minute);
                editor.putBoolean(getString(R.string.prefs_kegel_active), true);
                editor.apply();

                List<Calendar> calendars = new ArrayList<Calendar>();

                Calendar calendar1 = Calendar.getInstance();
                calendar1.set(Calendar.HOUR_OF_DAY, prefs.getInt(getString(R.string.prefs_first_alarm_hour), 0));
                calendar1.set(Calendar.MINUTE, prefs.getInt(getString(R.string.prefs_first_alarm_minute), 0));
                calendar1.set(Calendar.SECOND, 0);
                if (calendar1.compareTo(Calendar.getInstance()) < 0)
                {
                    calendar1.add(Calendar.DAY_OF_YEAR, 1);
                }
                calendars.add(calendar1);

                Calendar calendar2 = Calendar.getInstance();
                calendar2.set(Calendar.HOUR_OF_DAY, prefs.getInt(getString(R.string.prefs_second_alarm_hour), 0));
                calendar2.set(Calendar.MINUTE, prefs.getInt(getString(R.string.prefs_second_alarm_minute), 0));
                calendar2.set(Calendar.SECOND, 0);
                if (calendar2.compareTo(Calendar.getInstance()) < 0)
                {
                    calendar2.add(Calendar.DAY_OF_YEAR, 1);
                }
                calendars.add(calendar2);

                Calendar calendar3 = Calendar.getInstance();
                calendar3.set(Calendar.HOUR_OF_DAY, prefs.getInt(getString(R.string.prefs_third_alarm_hour), 0));
                calendar3.set(Calendar.MINUTE, prefs.getInt(getString(R.string.prefs_third_alarm_minute), 0));
                calendar3.set(Calendar.SECOND, 0);
                if (calendar3.compareTo(Calendar.getInstance()) < 0)
                {
                    calendar3.add(Calendar.DAY_OF_YEAR, 1);
                }
                calendars.add(calendar3);

                Context context = KegelThirdAlarmActivity.this;
                AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

                if (true) // DEBUG
                {
                    calendars = new ArrayList<>();
                    Calendar calendar = Calendar.getInstance();
                    calendars.add(calendar);
                }

                for (Calendar calendar : calendars) {
                    PendingIntent pi = PendingIntent.getBroadcast(context, 123456789 /*dunno lol*/,
                            new Intent(context, AlarmReceiver.class), PendingIntent.FLAG_UPDATE_CURRENT);
                    am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                            AlarmManager.INTERVAL_DAY, pi);
                }

                Intent intent = new Intent(KegelThirdAlarmActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();

            }
        });
    }

}
