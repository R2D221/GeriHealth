package mx.itesm.gerihealth;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {

            // TODO: Alarms get erased on reboot. Register them here again.

//            Intent serviceIntent = new Intent("com.myapp.NotifyService");
//            context.startService(serviceIntent);
        }
    }
}
