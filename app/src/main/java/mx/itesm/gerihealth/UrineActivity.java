package mx.itesm.gerihealth;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class UrineActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_urine);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SharedPreferences prefs = getSharedPreferences(getString(R.string.prefs), 0);

        TextView welcome = (TextView)findViewById(R.id.urine_welcome);

        welcome.setText(getString(R.string.urine_welcome, prefs.getString(getString(R.string.prefs_name), null)));

        //if (!prefs.getBoolean(getString(R.string.prefs_kegel_active), false)) {
            findViewById(R.id.urine_kegel).setVisibility(View.GONE);
            findViewById(R.id.urine_activate_kegel).setVisibility(View.VISIBLE);
        //}

        Button kegel = (Button) findViewById(R.id.urine_activate_kegel);
        kegel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UrineActivity.this, KegelFirstAlarmActivity.class);
                startActivity(intent);
            }
        });
        Button Register = (Button) findViewById(R.id.RegisterEmission);
        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UrineActivity.this, RegisterEmissionActivity.class);
                startActivity(intent);
            }
        });
        Button Register2 = (Button) findViewById(R.id.TimeRegister);
        Register2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UrineActivity.this, TimeRegisterActivity.class);
                startActivity(intent);
            }
        });
    }
}
