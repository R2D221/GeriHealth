package mx.itesm.gerihealth;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class TimeRegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_register);

        Button WakeUpButton = (Button) findViewById(R.id.WakeUp);
        WakeUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = getApplicationContext();
                CharSequence text = "¡Buenos días! Registraste tu hora de despertar";
                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context,text,duration);
                toast.show();

                Intent intent = new Intent(TimeRegisterActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
        Button SleepButton = (Button) findViewById(R.id.Sleep);
        SleepButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                Context context = getApplicationContext();
                CharSequence text = "Registraste tu hora de dormir, ¡que descanses!";
                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context,text,duration);
                toast.show();

                Intent intent = new Intent(TimeRegisterActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
    }
}
