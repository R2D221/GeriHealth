package mx.itesm.gerihealth;

import android.os.PowerManager;

/**
 * Created by r2d2a on 07/01/2016.
 */
public final class Globals {
    private Globals() { }

    public static PowerManager.WakeLock wakeLock;
}
