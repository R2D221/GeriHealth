package mx.itesm.gerihealth;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class KegelStartExerciseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kegel_start_exercise);

        final MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.alarm);
        mp.setLooping(true);
        mp.start();

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        Globals.wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "ALARM");

        Globals.wakeLock.acquire();

        Button kegel = (Button)findViewById(R.id.kegel_start_exercise);

        kegel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mp.stop();

                Intent intent = new Intent(KegelStartExerciseActivity.this, KegelExerciseActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

    }
}
