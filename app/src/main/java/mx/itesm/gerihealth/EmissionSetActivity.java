package mx.itesm.gerihealth;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class EmissionSetActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emission_set);

        Button Voluntary = (Button) findViewById(R.id.VolButton);
        Voluntary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EmissionSetActivity.this, VoluntaryVolume.class);
                startActivity(intent);
            }
        });
        Button Involuntary = (Button) findViewById(R.id.InvolButton);
        Involuntary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (EmissionSetActivity.this, InvoluntaryVolume.class);
                startActivity(intent);
            }
        });
    }
}
